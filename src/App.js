import React, { Component } from 'react';
import './App.css';
import Vote from './components/vote'
import ErrorComponent from './components/shared/errorComponent'
import InfoPage from './components/infoPage'
import Question from './components/question'
import MainMenu from './components/mainMenu'
import { BrowserRouter as Router, Route, Switch } from "react-router-dom"
import bulletTrain from "bullet-train-client"
import { voteOnQuestion, streamResults } from './services/questions'
import VerticalLayoutComponent from './components/shared/verticalLayout';
/**
 * vote
 * vote.voteNumber
 * vote.results
 */
class App extends Component {
  constructor(props) {
    super(props)
    const environmentID = (process.env.NODE_ENV === 'development') ? "RQGucLHch4aUP9DjLmWnkT" : "AdT9NyWjbFboJSa85XFBAB"
    this.state = {
      back_button_removal: false,
      submit_button_removal : false,
      pretty_info_links: false
    }
    bulletTrain.identify(window.navigator.platform)
    bulletTrain.init({
      environmentID: environmentID,
      onChange: () => {
        //Features have been retrieved from bullet train, either from the server or Local AsyncStorage
        this.setState({
          back_button_removal: bulletTrain.hasFeature('back_button_removal'),
          pretty_info_links: bulletTrain.hasFeature('updated_info_links'),
          submit_button_removal: bulletTrain.hasFeature('submit_button_removal')
        })

      }
    });
  }

  triggerUsabillaPageView = () => window.usabilla_live('virtualPageView')
  render() {

    return (
      <ErrorComponent>
        <VerticalLayoutComponent classes="App container align-items-center">
          <Router>
            <Switch>
              <Route onEnter={this.triggerUsabillaPageView} path="/info" component={(props) => <InfoPage {...props} features={{ pretty_info_links: this.state.pretty_info_links }} />} />
              <Route onEnter={this.triggerUsabillaPageView} path="/polls" component={Question} />
              <Route onEnter={this.triggerUsabillaPageView} path="/:voteName" component={(props) => <Vote {...props} service={{ voteOnQuestion, streamResults }} features={{ back_button_removal: this.state.back_button_removal, submit_button_removal: this.state.submit_button_removal }} />} />
              <Route onEnter={this.triggerUsabillaPageView} exact path="/" component={MainMenu} />
            </Switch>
          </Router>
        </VerticalLayoutComponent>
      </ErrorComponent>
    );
  }
}

export default App;
