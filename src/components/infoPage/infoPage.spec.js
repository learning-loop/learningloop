import React from 'react';
import { shallow } from 'enzyme';
import InfoPage from '.'


describe('', () => {
    let wrapper

    beforeEach(() => {

        wrapper = shallow(<InfoPage  features={{ pretty_info_links: true}} />);

    })
    it('should render with an element containing id', () => {
        expect(wrapper.find('h5#title')).toHaveLength(2)
    })

    it('should have an unstyled list', () => {
        expect(wrapper.find('ul.list-unstyled')).toHaveLength(2)
    })
})