import React from 'react'
import { Link } from "react-router-dom"
import VerticalLayoutComponent from '../shared/verticalLayout'

class MainMenu extends React.Component {
    constructor(props) {
        super(props)
        this.state = { learningLoopJoin: 'https://gitlab.com/learning-loop' }
    }
    render() {
        return (
            <VerticalLayoutComponent classes="h-100">
                <Link className="btn btn-primary my-4" id="create-poll-btn" to="/polls">Create a Fist To Five Poll</Link>
                <Link className="btn btn-primary my-4" to="/info" id="info-btn">What is a Fist to Five Poll?</Link>
                <a className="btn btn-primary my-4" href={this.state.learningLoopJoin} id="join-btn">Join this Open Source Project</a>
            </VerticalLayoutComponent>
        )
    }
}

export default MainMenu