import React from 'react';
import { mount } from 'enzyme';
import MainMenu from '.'
import { Link, MemoryRouter } from "react-router-dom"


describe('', () => {
    let wrapper
    beforeEach(() => {

        wrapper = mount(<MemoryRouter><MainMenu /></MemoryRouter>);

    })

    afterEach(() => {
        wrapper.unmount()
        wrapper = null
    })
    it('should render with 2 Link components', () => {
        //expect(wrapper.find('div')).toHaveLength(1)
        expect(wrapper.find(Link)).toHaveLength(2)
        expect(wrapper.find('a')).toHaveLength(3)
    })

    it('THEN renders first Link with text for creating a poll', () => {
        expect(wrapper.find('a#create-poll-btn')).toHaveLength(1)
        expect(wrapper.find(Link).at(0).prop('to')).toEqual('/polls')
        expect(wrapper.find(Link).at(0).text()).toEqual('Create a Fist To Five Poll')
    })

    it('THEN renders second with a link that takes you to info', () => {
        expect(wrapper.find('a#info-btn')).toHaveLength(1)
        expect(wrapper.find(Link).at(1).prop('to')).toEqual('/info')
        expect(wrapper.find(Link).at(1).text()).toEqual('What is a Fist to Five Poll?')
    })
    it('THEN renders third link that takes you to the gitlab repo', () => {
        expect(wrapper.find('a#join-btn')).toHaveLength(1)
        expect(wrapper.find('a#join-btn').prop('href')).toEqual('https://gitlab.com/learning-loop')
        expect(wrapper.find('a#join-btn').text()).toEqual('Join this Open Source Project')
    })
})