import React from 'react'
import VerticalLayoutComponent from '../../shared/verticalLayout'

class CreateQuestion extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            text: ''
        }
    }

    handleTextInput = (e) => {
        e.preventDefault()
        this.setState({ text: e.target.value })
    }

    createPoll = async (e) => {
        //window.dataLayer.push({ 'somethingRandom': 'another random thing' });
        return await this.props.createQuestion({ text: this.state.text })
    }
    render() {
        return (
            <VerticalLayoutComponent id="poll-creator" >
                <h3 className="text-center my-3" id="poll-creation-title">Enter a Poll Title</h3>
                <div className="input-group my-3">
                    <textarea className={"border border-primary container-fluid"} value={this.state.text} onChange={this.handleTextInput} maxLength="255" id="poll-question" rows="5" ></textarea>
                </div>
                <button id="submit-poll-btn" className="btn btn-primary my-3" type="button" onClick={this.createPoll} >Create Poll</button>
            </VerticalLayoutComponent>
        )
    }
}
export default CreateQuestion