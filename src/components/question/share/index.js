import React from 'react'
import { Link } from "react-router-dom"
import VerticalLayoutComponent from '../../shared/verticalLayout'


class ShareQuestion extends React.Component {
    render() {
        return (
            <VerticalLayoutComponent id="share-poll">
                <div>
                    <h3 id="share-screen-title" className="text-center my-4" >Poll Created</h3>
                    <p id="poll-topic" className="text-center font-weight-bold mx-4 mb-4">{this.props.question.text}</p>
                </div>
                <div>
                    <h4 id="poll-url-link" className="text-center text-primary mx-3 mb-4">{`${window.location.protocol}//${window.location.hostname}/${this.props.question ? this.props.question.id : ''}`}</h4>
                    <p id="share-instructions" className="text-center font-weight-bold mx-4 mb-4">Copy this link and share with others to join your poll</p>
                </div>
                <div>
                    <Link id="go-to-poll-btn" to={`/${this.props.question ? this.props.question.id : ''}`} className="btn btn-primary btn-block">Go to Poll Now</Link>
                </div>
            </VerticalLayoutComponent>
        )
    }
}
export default ShareQuestion