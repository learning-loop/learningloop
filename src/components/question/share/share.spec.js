import React from 'react';
import { shallow } from 'enzyme';
import ShareQuestion from '.'
import { Link } from "react-router-dom"



describe('should show link info for a created poll', () => {
    let wrapper, props
    beforeEach(() => {
        props = {
            question: {
                text: 'something to talk about',
                id: 'random'
            }
        }
        wrapper = shallow(<ShareQuestion {...props} />);
    })

    it('should render the share poll component', () => {
        expect(wrapper.find('#share-poll')).toHaveLength(1)
    })
    it('should render text of the topic, url link that and instructions to share, and button that will go to poll directly', () => {
        expect(wrapper.find('h3')).toHaveLength(1)
        expect(wrapper.find('h3#share-screen-title').text()).toEqual('Poll Created')
        expect(wrapper.find('p#poll-topic').text()).toEqual('something to talk about')

    })

    it('should render url link that and instructions to share', () => {
        expect(wrapper.find('#poll-url-link')).toHaveLength(1)
        expect(wrapper.find('#poll-url-link').text()).toEqual('http://localhost/random')
        expect(wrapper.find('p#share-instructions')).toHaveLength(1)
        expect(wrapper.find('p#share-instructions').text()).toEqual('Copy this link and share with others to join your poll')
    })

    it('should render button that will go to poll directly', () => {
        expect(wrapper.find(Link)).toHaveLength(1)
        expect(wrapper.find(Link).props().to).toEqual('/random')
    })
})