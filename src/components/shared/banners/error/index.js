import React from 'react'
import './errorBanner.css'
let ErrorBanner = props => (<div id="error-banner" className="error-banner text-center" role="alert">{props.message}</div>)

export default ErrorBanner