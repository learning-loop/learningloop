import React from 'react';
import { shallow } from 'enzyme';
import ErrorComponent from '.';
import ErrorBanner from '../banners/error'

let ProblemComponent = () => null

describe('', () => {
    let wrapper, props
    beforeEach(() => {
        wrapper = shallow(<ErrorComponent  {...props}><ProblemComponent /></ErrorComponent>);
        props = {
            children: []
        }
    })
    describe('WHEN an error has not occurred', () => {
        it('THEN return the child components', () => {
            expect(wrapper.state().hasError).toBe(false)
            expect(wrapper.find(ProblemComponent)).toHaveLength(1)
        })
    })


    describe('WHEN an error occurrs within a child component', () => {
        it('THEN render error banner', () => {

            wrapper.simulateError(new Error('BOOM'))
            expect(wrapper.state().hasError).toBe(true)
            expect(wrapper.find(ProblemComponent)).toHaveLength(1)
            expect(wrapper.find(ErrorBanner)).toHaveLength(1)
            expect(wrapper.find(ErrorBanner).props().message).toEqual('Something went wrong. Please refresh page')
        })
    })
})