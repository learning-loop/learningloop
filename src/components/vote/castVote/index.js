import React, { Component } from 'react';
import VoteButton from '../../voteButton'
import VerticalLayoutComponent from '../../shared/verticalLayout'

class CastVoteComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
            selectedVote: '',
            startVoting: new Date()
        }
    }

    selectVoteOption = async (e) => {
        if(this.props.features.submit_button_removal){
            await this.setState({ selectedVote: e.target.value })
            let endVote = new Date()
            return await this.props.submitVote({
                level: this.state.selectedVote,
                timeToVote: (endVote.getTime() - this.state.startVoting.getTime())
            })
        }
        else{
            this.setState({ selectedVote: e.target.value })
            return
        }
    }

    isActive = (vote) => this.state.selectedVote === vote ? 'active' : ''

    submitVote = async () => {
        if (this.state.selectedVote) {
            let endVote = new Date()
            return await this.props.submitVote({
                level: this.state.selectedVote,
                timeToVote: (endVote.getTime() - this.state.startVoting.getTime())
            })
        }
    }

    render() {
        return (
            <VerticalLayoutComponent id="vote-page" classes=" align-items-center w-75">
                {this.props.options.map(vote => {
                    return (<VoteButton key={vote} active={this.isActive(vote)} handleChange={this.selectVoteOption} voteNumber={vote} />)
                })}
                {this.props.features.submit_button_removal ? '' : <button id="submit-vote-btn" className={`btn btn-primary btn-block mt-5 ${this.state.selectedVote ? '' : 'disabled'}`} type="button" onClick={this.submitVote}>Submit Vote</button>}
            </VerticalLayoutComponent>
        )
    }
}

export default CastVoteComponent;
