import React from 'react';
import { shallow } from 'enzyme';
import VoteComponent from '.';
import CastVoteComponent from './castVote'
import VoteResultsComponent from './results'

describe('when A voter opens the voting selection screen', () => {
    let wrapper, props, question
    beforeEach(() => {
        question = {
            "text": "Something To Ask",
            "id": "24FWYU40",
            "voteLevel": 5,
            "voteCount": 3,
            "results": { "0": 0, "1": 0, "2": 0, "3": 0, "4": 0, "5": 3 }
        }
        props = {
            match: {
                params: {
                    voteName: 'random'
                }
            },
            service: {
                voteOnQuestion: jest.fn().mockResolvedValue({}),
                streamResults: jest.fn().mockReturnValue({})
            }
        }
        wrapper = shallow(<VoteComponent {...props} />);
        wrapper.setState({ question: question })
    })

    it('should render without crashing', () => {
        expect(wrapper.find(CastVoteComponent)).toHaveLength(1)
    })

    it('should have a header containing the question text', () => {
        expect(wrapper.find('#question-text')).toHaveLength(1)
        expect(wrapper.find('#question-text').text()).toEqual(question.text)

    })

    it('should display results after vote has been made', () => {
        wrapper.setState({ showResults: true })
        expect(wrapper.find(CastVoteComponent)).toHaveLength(0)
        expect(wrapper.find(VoteResultsComponent)).toHaveLength(1)
    })

    it('should start streaming the question information', () => {
        expect(props.service.streamResults).toBeCalled()
    })
    it('should submit vote when submitVote is clicked', () => {
        wrapper.find(CastVoteComponent).props().submitVote({ timeToVote: '', level: 3 })
        //expect(props.service.voteOnQuestion).toBeCalled()
    })

    it('should take user back to vote if back button clicked on result page', () => {
        wrapper.setState({ showResults: true })

        wrapper.find(VoteResultsComponent).props().back()

        expect(wrapper.state().showResults).toBe(false)
    })
})