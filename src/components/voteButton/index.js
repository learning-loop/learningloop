
import React from 'react';
import './voteButton.css'
let VoteButton = props => {
    return (
        <label id={`${props.voteNumber}-vote-btn`} htmlFor={`vote_${props.voteNumber}`} className={`btn btn-primary fof-button ${props.active}`}>
            <input className='radio-button' id={`vote_${props.voteNumber}`} type="radio" value={props.voteNumber} onChange={props.handleChange} name='vote' />{props.voteNumber}
        </label>
    )
}

export default VoteButton