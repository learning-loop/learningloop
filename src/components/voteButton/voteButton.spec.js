import React from 'react';
import { shallow } from 'enzyme';
import VoteButton from '.';


describe('GIVEN A voter opens the vote entry page', () => {
    let wrapper, props
    beforeEach(() => {
        props = {
            handleChange: jest.fn(),
            voteNumber: 1,
            style: {
                something: 'hello'
            },
        }
        wrapper = shallow(<VoteButton {...props} />);
    })
    describe('WHEN the button rendors', () => {
        it('THEN it renders without crashing', () => {
            expect(wrapper.find('#vote_1')).toHaveLength(1)
            expect(wrapper.find('.fof-button')).toHaveLength(1)
        })
        it('THEN it will add active value to class name for label', () => {
            expect(wrapper.find('label.fof-button.active')).toHaveLength(0)
            wrapper.setProps({ ...props, active: 'active' })
            expect(wrapper.find('label.fof-button.active')).toHaveLength(1)
        })
    })
    describe('WHEN The voter selects the button', () => {
        it('THEN execute passed in handle change fn', () => {
            wrapper.find('input#vote_1').simulate('change')
            expect(props.handleChange).toBeCalled()
        })
    })
})