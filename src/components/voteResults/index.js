
import React from 'react';
import VoteButton from '../voteButton';
let VoteResults = props => {
    // d-flex justify-content-around align-items-center
    return (
        <div className="row align-items-center w-100" id={`vote_results_${props.option}`}>
            <div className="col">
                <VoteButton voteNumber={props.option} active={'disabled text-center'} />
            </div>
            <div className="col">
                <h4 className="text-center">{props.results}</h4>
            </div>
        </div>
    )
}

export default VoteResults